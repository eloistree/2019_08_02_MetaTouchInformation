# How to use: 2019_08_02_MetaTouchInformation   
   
Add the following line to the [UnityRoot]/Packages/manifest.json    
``` json     
"be.eloiexperiments.metatouchinformation":"",    
```    
--------------------------------------    
   
Feel free to support my work: http://patreon.com/eloistree   
Contact me if you need assistance: http://eloistree.page.link/discord   
   
--------------------------------------    
``` json     
{                                                                                
  "name": "be.eloiexperiments.metatouchinformation",                              
  "displayName": "MetaTouchInformation",                        
  "version": "0.0.1",                         
  "unity": "2018.1",                             
  "description": "Save and view meta information received based on the position of the touch in a guardian system of Oculus",                         
  "keywords": ["Script","Tool","Touch","Data Collecting"],                       
  "category": "Script",                   
  "dependencies":{"be.eloiexperiments.oculusguardianidentity": "0.0.1"}     
  }                                                                                
```    